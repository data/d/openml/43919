# OpenML dataset: airfoil_self_noise

https://www.openml.org/d/43919

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Citation**

Please refer to UCI's [citation policy](https://archive.ics.uci.edu/ml/citation_policy.html).

**Source**

Donor: Dr Roberto Lopez robertolopez '@' intelnics.com Intelnics

Creators:
Thomas F. Brooks, D. Stuart Pope and Michael A. Marcolini NASA

Note that the names of the columns have been altered to be more descriptive.

**Data Description**

The NASA data set comprises different size NACA 0012 airfoils at various wind tunnel speeds and angles of attack. The span of the airfoil and the observer position were the same in all of the experiments.

**Attribute description**

This problem has the following inputs:

* frequency - Frequency, in Hertzs.
* angle -  Angle of attack, in degrees.
* length - Chord length, in meters.
* velocity - Free-stream velocity, in meters per second.
* thickness - Suction side displacement thickness, in meters.

The only output is:

* pressure - Scaled sound pressure level, in decibels.

Relevant Papers:

T.F. Brooks, D.S. Pope, and A.M. Marcolini.
Airfoil self-noise and prediction.
Technical report, NASA RP-1218, July 1989.

K. Lau.
A neural networks approach for aerofoil noise prediction.
Master thesis, Department of Aeronautics.
Imperial College of Science, Technology and Medicine (London, United Kingdom), 2006.

R. Lopez.
Neural Networks for Variational Problems in Engineering.
PhD Thesis, Technical University of Catalonia, 2008.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43919) of an [OpenML dataset](https://www.openml.org/d/43919). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43919/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43919/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43919/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

